﻿using System;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;

namespace RogueLikeEditor
{
    public partial class LevelEditor : Form
    {
        GraphicsDevice device;

        public LevelEditor()
        {
            InitializeComponent();
        }

        internal void InitializeGame()
        {
            PresentationParameters pp = new PresentationParameters();
            pp.BackBufferFormat = SurfaceFormat.Color;
            pp.BackBufferHeight = 600;
            pp.BackBufferWidth = 800;
            pp.DeviceWindowHandle = pboxViewer.Handle;
            pp.IsFullScreen = false;
            device = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.Reach, pp);
        }

        internal void Render()
        {
            device.Clear(Microsoft.Xna.Framework.Color.CornflowerBlue);
            device.Present();
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
