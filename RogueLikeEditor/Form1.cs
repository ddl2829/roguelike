﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RogueLikeEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            btnRoom.Click += new EventHandler(btnRoom_Click);
            btnLevel.Click += new EventHandler(btnLevel_Click);
        }

        void btnLevel_Click(object sender, EventArgs e)
        {
            using (LevelEditor levelForm = new LevelEditor())
            {
                Application.EnableVisualStyles();
                levelForm.InitializeGame();
                levelForm.Show();
                while (levelForm.Created)
                {
                    levelForm.Render();
                    Application.DoEvents();
                }
            }
        }

        public void btnRoom_Click(object sender, EventArgs e)
        {
            using (RoomForm roomForm = new RoomForm())
            {
                roomForm.ShowDialog();
            }
        }
    }
}
