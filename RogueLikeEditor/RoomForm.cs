﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XRogueLibrary;

namespace RogueLikeEditor
{
    public partial class RoomForm : Form
    {
        DungeonRoom ThisRoom;

        public RoomForm()
        {
            InitializeComponent();
            btnLoad.Click += new EventHandler(btnLoad_Click);
            btnSave.Click += new EventHandler(btnSave_Click);
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            ThisRoom = new DungeonRoom(tbMesh.Text, float.Parse(tbScale.Text), float.Parse(tbRotation.Text));
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.Filter = "XML Files (*.xml)|*.xml|All files(*.*)|*.*";
                dialog.FilterIndex = 0;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    XnaSerializer.Serialize<DungeonRoom>(dialog.FileName, ThisRoom);
                }
            }
        }

        void btnLoad_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = "XML Files (*.xml)|*.xml|All files(*.*)|*.*";
                dialog.FilterIndex = 0;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    ThisRoom = XnaSerializer.Deserialize<DungeonRoom>(dialog.FileName);
                    tbMesh.Text = ThisRoom.Mesh;
                    tbRotation.Text = String.Format("{0}", ThisRoom.Base_Rotation);
                    tbScale.Text = String.Format("{0}", ThisRoom.Scale);
                }
            }
        }
    }
}
