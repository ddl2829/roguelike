#define MaxBones 59

float4x4 Bones[MaxBones];
float4x4 World;
float4x4 View;
float4x4 Projection;
Texture ModelTexture;
float3 LightPosition;
float Attenuation;
float Falloff;

sampler ColoredTextureSampler = sampler_state {
	texture = <ModelTexture>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
	float3 Normal : NORMAL0;
	float3 Binormal: BINORMAL0;
	float3 Tangent : TANGENT0;
	float2 TexCoord : TEXCOORD0;
	float4 BoneIndices : BLENDINDICES0;
	float4 BoneWeights : BLENDWEIGHT0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float4 Pos2 : TEXCOORD0;
	float2 textureCoordinates : TEXCOORD1;
};

VertexShaderOutput AnimatedVertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	float4x4 skinTransform = 0;
	skinTransform += Bones[input.BoneIndices.x] * input.BoneWeights.x;
	skinTransform += Bones[input.BoneIndices.y] * input.BoneWeights.y;
	skinTransform += Bones[input.BoneIndices.z] * input.BoneWeights.z;
	skinTransform += Bones[input.BoneIndices.w] * input.BoneWeights.w;

	float4 worldPosition = mul(input.Position, skinTransform);

	output.Position = mul(mul(mul(worldPosition, World), View), Projection);
    output.textureCoordinates = input.TexCoord;
	output.Pos2 = output.Position;
    return output;
}

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	output.Position = mul(mul(mul(input.Position, World), View), Projection);
    output.textureCoordinates = input.TexCoord;
	output.Pos2 = output.Position;
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	float4 output = tex2D(ColoredTextureSampler, input.textureCoordinates);
	float dp = distance(input.Pos2, LightPosition);
	float katt = 1 - pow(clamp(dp / Attenuation, 0, 1), Falloff);
	
	float4 ambientColor = float4(1, 1, 1, 1);

    return (float4)0 + output * katt;
}

technique PointLight_Animated
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 AnimatedVertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}

technique PointLight_World
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}