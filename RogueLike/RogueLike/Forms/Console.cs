﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RogueLike.Forms
{
    public partial class Console : Form
    {
        public string input = "";

        public Console()
        {
            InitializeComponent();
            btnSend.Click += new EventHandler(btnSend_Click);
        }

        void btnSend_Click(object sender, EventArgs e)
        {
            input = tbConsole.Text;
            this.Close();
        }
    }
}
