using System.Diagnostics;
using System.Windows.Forms;
using GameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueLike.Components;
using RogueLike.GameScreens;

namespace RogueLike
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public Rectangle ScreenRectangle = new Rectangle(0, 0, 800, 600);
        public SpriteBatch spriteBatch;

        GraphicsDeviceManager graphics;
        
        //Game Screens
        GameStateManager StateManager;
        public TitleScreen TitleScreen;
        public ActionScreen ActionScreen;
        public GameOverScreen GameOverScreen;
        public MenuScreen MenuScreen;

        //Mouse Pointer
        public bool PointerVisible = true;
        Texture2D PointerTexture;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = ScreenRectangle.Width;
            graphics.PreferredBackBufferHeight = ScreenRectangle.Height;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Components.Add(new InputHandler(this));
            Components.Add(new ScriptEngine(this));

            StateManager = new GameStateManager(this);
            Components.Add(StateManager);
                    
            ActionScreen = new ActionScreen(this, StateManager);
            TitleScreen = new TitleScreen(this, StateManager);
            GameOverScreen = new GameOverScreen(this, StateManager);
            MenuScreen = new MenuScreen(this, StateManager);

            StateManager.ChangeState(ActionScreen);
            ActionScreen.AcceptInput = false;
            StateManager.PushState(TitleScreen);

            spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), spriteBatch);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            PointerTexture = Content.Load<Texture2D>(@"Textures\UI\pointers");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public void CheatConsole()
        {
            using(RogueLike.Forms.Console console = new RogueLike.Forms.Console()) 
            {
                console.Show();
                while (console.Created)
                {
                    Application.DoEvents();
                }
                if (console.input != "")
                {
                    Debug.Print("{0}", ScriptEngine.RunStatic(console.input));
                }
            }
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);
            if (PointerVisible)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(PointerTexture, new Rectangle((int)InputHandler.MousePosition().X, (int)InputHandler.MousePosition().Y, 19, 19), new Rectangle(0, 0, 19, 19), Color.White);
                spriteBatch.End();
            }
        }
    }
}
