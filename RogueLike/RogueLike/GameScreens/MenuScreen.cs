﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using GameLibrary.Controls;

namespace RogueLike.GameScreens
{
    public class MenuScreen : BaseGameScreen
    {
        LinkLabel exitLabel;
        LinkLabel consoleLabel;
        LinkLabel continueLabel;

        public MenuScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            continueLabel = new LinkLabel();
            continueLabel.Text = "Continue";
            continueLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - continueLabel.SpriteFont.MeasureString(continueLabel.Text).X) / 2, GameRef.ScreenRectangle.Height / 2 - 30);
            continueLabel.Color = Color.White;
            continueLabel.HasFocus = true;
            continueLabel.Selected += new EventHandler(continueLabel_Selected);
            ControlManager.Add(continueLabel);

            consoleLabel = new LinkLabel();
            consoleLabel.Text = "Console";
            consoleLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - consoleLabel.SpriteFont.MeasureString(consoleLabel.Text).X) / 2, GameRef.ScreenRectangle.Height / 2);
            consoleLabel.Color = Color.White;
            consoleLabel.HasFocus = false;
            consoleLabel.Selected += new EventHandler(consoleLabel_Selected);
            ControlManager.Add(consoleLabel);

            exitLabel = new LinkLabel();
            exitLabel.Text = "Quit";
            exitLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - exitLabel.SpriteFont.MeasureString(exitLabel.Text).X) / 2, GameRef.ScreenRectangle.Height / 2 + 30);
            exitLabel.Color = Color.White;
            exitLabel.Selected += new EventHandler(exitLabel_Selected);
            ControlManager.Add(exitLabel);
        }

        void continueLabel_Selected(object sender, EventArgs e)
        {
            GameRef.ActionScreen.AcceptInput = true;
            StateManager.PopState();
        }

        void consoleLabel_Selected(object sender, EventArgs e)
        {
            GameRef.CheatConsole();
        }

        void exitLabel_Selected(object sender, EventArgs e)
        {
            GameRef.Exit();
        }

        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime);
            if (InputHandler.KeyPressed(Keys.Escape))
            {
                GameRef.ActionScreen.AcceptInput = true;
                StateManager.PopState();
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.spriteBatch.Begin();
            ControlManager.Draw(GameRef.spriteBatch);
            GameRef.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
