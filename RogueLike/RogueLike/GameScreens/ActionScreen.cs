﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using GameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Roguelike.Components;
using RogueLike.Components;
using RogueLike.Components.Helpers;
using RogueLike.Components.NpcClasses;
using XRogueLibrary;

namespace RogueLike.GameScreens
{
    /// <summary>
    /// The ActionScreen is where the main game occurs
    /// </summary>
    public class ActionScreen : BaseGameScreen
    {
        RogueCamera Camera;
        Player Player;
        World World;
        ScriptEngine scriptEngine;
        public List<Notification> notifications;
        SpriteFont notificationFont;
        Texture2D HealthBar;
        Texture2D lifeHeart;

        Texture2D Overlay;
        Texture2D HealthSphere;
        Texture2D StaminaSphere;
        Texture2D SphereBack;
        Texture2D Key;
        Texture2D NoKey;
        int HealthColorAngle = 0;
        int StaminaColorAngle = 0;

        public ActionScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {
        }

        public override void Initialize()
        {
            notifications = new List<Notification>();

            World = new World(GameRef);
            Components.Add(World);
            World.ActorDeath += new EventHandler(World_ActorDeath);

            scriptEngine = new ScriptEngine(GameRef);
            Components.Add(scriptEngine);

            Player = new Player(Game, "knight");
            Player.screenSize = new Vector2(GameRef.ScreenRectangle.Width, GameRef.ScreenRectangle.Height);
            Player.OnAction += new EventHandler(Player_OnAction);
            Player.OnDeath += new EventHandler(Player_OnDeath);
            Player.OnNoLivesLeft += new EventHandler(Player_OnNoLivesLeft);
            Components.Add(Player);

            Camera = new RogueCamera(Game, Player);
            Components.Add(Camera);

            base.Initialize();
            World.Add(Player.Controller.BepuController);
        }

        void Player_OnNoLivesLeft(object sender, EventArgs e)
        {
            AcceptInput = false;
            StateManager.PushState(GameRef.GameOverScreen);
        }

        void Player_OnDeath(object sender, EventArgs e)
        {
            World.theWorld.Remove(Player.Controller.BepuController);
            Player.Respawn(Player.PlayerRespawnPosition);
            Player.accept_input_private = true;
            World.theWorld.Add(Player.Controller.BepuController);
        }

        void Player_OnAction(object sender, EventArgs e)
        {
            foreach (EventNode node in World.currentLevel.events.FindAll(n => n.Type == 2 && (!n.ran || (n.ran && !n.runOnce))))
            {
                if (Player.Controller.BepuController.Body.CollisionInformation.BoundingBox.Intersects(node.BoundingSphere))
                {
                    bool success = (bool)scriptEngine.Run(node.onAction);
                    if (success)
                        node.ran = true;
                }
            }
        }

        /// <summary>
        /// This event is copied all the way up the class heirarchy to here to remove any scripts referencing this npc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void World_ActorDeath(object sender, System.EventArgs e)
        {
            //Get the NPC list index of the dead actor
            NpcBase actor = (NpcBase)sender;
            actor.RemoveHitSubscription(Player);
            int index = -1;
            foreach (int key in World.NpcList.Keys)
            {
                if (World.NpcList[key] == actor)
                {
                    index = key;
                    break;
                }
            }
            if (index != -1)
            {
                //Use a regex to search all script strings for a reference to this actor
                Regex r = new Regex("NpcList\\[" + index + "\\]");
                for (int i = scriptEngine.globalScripts.Count - 1; i >= 0; --i)
                {
                    //When the reference is found remove the script
                    if (r.IsMatch(scriptEngine.globalScripts[i]))
                    {
                        scriptEngine.globalScripts.RemoveAt(i);
                    }
                }
                //Finally remove the npc from the world
                World.NpcList.Remove(index);
            }
            World.theWorld.Remove(actor.Controller.BepuController);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            lifeHeart = Game.Content.Load<Texture2D>(@"Textures\UI\life");
            notificationFont = Game.Content.Load<SpriteFont>(@"Fonts\NotificationFont");
            HealthBar = Game.Content.Load<Texture2D>(@"Textures\UI\HP");

            Overlay = Game.Content.Load<Texture2D>(@"Textures\UI\overlay");
            HealthSphere = Game.Content.Load<Texture2D>(@"Textures\UI\hp_sphere");
            StaminaSphere = Game.Content.Load<Texture2D>(@"Textures\UI\stamina_sphere");
            SphereBack = Game.Content.Load<Texture2D>(@"Textures\UI\sphere_back");
            Key = Game.Content.Load<Texture2D>(@"Textures\UI\key");
            NoKey = Game.Content.Load<Texture2D>(@"Textures\UI\nokey");
        }


        private int NextColorAngle(float percent, int angle)
        {
            if (percent < 0.33f)
            {
                angle += 20;
            }
            else if (percent < 0.66f)
            {
                angle += 10;
            }
            else
            {
                angle = 0;
            }
            return angle;
        }


        private Color ColorValue(int angle)
        {
            float value = (float)Math.Cos(MathHelper.ToRadians(angle));   //cosine outputs value between -1 and 1
            value = (value + 1) /2;    //keeps values a positive # between 0 and 1

            return new Color(value, value, value);
        }



        public override void Update(GameTime gameTime)
        {
            scriptEngine.SetParameter("NotificationList", notifications);
            scriptEngine.SetParameter("NpcList", World.NpcList);
            scriptEngine.SetParameter("Player", Player);
            scriptEngine.SetParameter("GlobalScripts", scriptEngine.globalScripts);
            scriptEngine.SetParameter("World", World);

            if (Player.Controller.Position.Y <= -1.0f)
            {
                World.theWorld.Remove(Player.Controller.BepuController);
                Player.Respawn(Player.PlayerRespawnPosition);
                World.theWorld.Add(Player.Controller.BepuController);
            }

            if (World.NpcList.Count == 0)
            {
                if (StateManager.CurrentState == this)
                {
                    GameRef.GameOverScreen.win = true;
                    AcceptInput = false;
                    StateManager.PushState(GameRef.GameOverScreen);
                }
            }

            for (int i = notifications.Count - 1; i >= 0; --i)
            {
                notifications[i].Update(gameTime);
                if (notifications[i].Expired)
                    notifications.RemoveAt(i);
            }
            if (!AcceptInput)
                Player.AcceptInput = false;
            else
                Player.AcceptInput = true;

            CheckLevelEventScripts(World.currentLevel);

            foreach (NpcBase npc in World.NpcList.Values)
            {
                if (Player.hitArea.Intersects(npc.Controller.BepuController.Body.CollisionInformation.BoundingBox))
                {
                    npc.SubscribeToHit(Player);
                }
                else
                {
                    npc.RemoveHitSubscription(Player);
                }
                if (npc.hitArea.Intersects(Player.Controller.BepuController.Body.CollisionInformation.BoundingBox))
                {
                    if (!npc.AttackSubscribed)
                    {
                        npc.OnAttack += Player.Npc_Attacked;
                    }
                }
                else
                {
                    if (npc.AttackSubscribed)
                    {
                        npc.OnAttack -= Player.Npc_Attacked;
                    }
                }
            }

            if (AcceptInput)
            {
                if (InputHandler.KeyPressed(Keys.Escape))
                {
                    this.AcceptInput = false;
                    Player.AcceptInput = false;
                    StateManager.PushState(GameRef.MenuScreen);
                }
                if (InputHandler.KeyPressed(Keys.Down))
                {
                    Camera.height = Camera.height - new Vector3(0, 0.5f, 0);
                }
                if (InputHandler.KeyPressed(Keys.Up))
                {
                    Camera.height = Camera.height + new Vector3(0, 0.5f, 0);
                }
            }


            base.Update(gameTime);
        }

        private void CheckLevelEventScripts(Level level)
        {
            foreach (EventNode node in level.events)
            {
                if (node.Type == 0 || node.Type == 1)
                {
                    if (node.runOnce && node.ran)
                        continue;
                    if (node.Type == 0)
                    {
                        if (Player.Controller.BepuController.Body.CollisionInformation.BoundingBox.Intersects(node.BoundingSphere) && !node.near)
                        {
#if DEBUG
                            Debug.Print("{0}", scriptEngine.Run(node.onEnter));
#else
                    scriptEngine.Run(node.onEnter);
#endif
                            node.near = true;
                            if (node.runOnce)
                                node.ran = true;
                        }
                    }
                    else
                    {
                        if (!Player.Controller.BepuController.Body.CollisionInformation.BoundingBox.Intersects(node.BoundingSphere) && node.near)
                        {
#if DEBUG
                            Debug.Print("{0}", scriptEngine.Run(node.onExit));
#else
                    scriptEngine.Run(node.onEnter);
#endif
                            node.near = false;
                            if (node.runOnce)
                                node.ran = true;
                        }
                    }
                }
                else if (node.Type == 2)
                {
                    //Do nothing
                }
                else
                {
                    throw new Exception("Invalid Node Type");
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            base.Draw(gameTime);

#if DEBUG
            foreach (EventNode node in World.currentLevel.events)
            {
                if ((node.runOnce && !node.ran) || !node.runOnce)
                    BoundingSphereRenderer.Render(node.BoundingSphere, GraphicsDevice, RogueCamera.View, RogueCamera.Projection, Color.Red);
            }
#endif

            GameRef.spriteBatch.Begin();

            //GameRef.spriteBatch.Draw(HealthBar, new Rectangle(5, 5, 204, 24), Color.Black);
            //GameRef.spriteBatch.Draw(HealthBar, new Rectangle(7, 7, (int)(200 * Player.PercentHealth), 20), Color.Red);

            //GameRef.spriteBatch.Draw(HealthBar, new Rectangle(5, 32, 204, 24), Color.Black);
            //GameRef.spriteBatch.Draw(HealthBar, new Rectangle(7, 34, (int)(200 * Player.PercentStamina), 20), Color.Green);


            //draw boss key
            if (Player.hasKey)
            {
                GameRef.spriteBatch.Draw(Key, new Vector2(150, 525), Color.White);
            }
            else
            {
                GameRef.spriteBatch.Draw(NoKey, new Vector2(150, 525), Color.White);
            }


            //XNA's y-axis in positive in the downward direction
            //so in order to draw only the bottom portion of an image, we need to
            //1) push the image down by texture height - vertical shift
            //2) draw the texture rectangle from the vertical shift

            //GameRef.spriteBatch.Draw(HealthSphere, new Vector2(0, 0), Color.White);

            //back side of the sphere
            GameRef.spriteBatch.Draw(
                SphereBack,
                new Vector2(
                    0,
                    GameRef.ScreenRectangle.Height - SphereBack.Height),
                Color.White);
            GameRef.spriteBatch.Draw(
                SphereBack,
                new Vector2(
                    GameRef.ScreenRectangle.Width - StaminaSphere.Width,
                    GameRef.ScreenRectangle.Height - SphereBack.Height),
                Color.White);

            //player hp sphere
            int HealthSphereAdjustedHeight = (int)(HealthSphere.Height * Player.PercentHealth);
            HealthColorAngle = NextColorAngle(Player.PercentHealth, HealthColorAngle);
            GameRef.spriteBatch.Draw(
                HealthSphere,
                new Vector2(
                    0,
                    GameRef.ScreenRectangle.Height - HealthSphereAdjustedHeight),  //shift image down based on player health
                new Rectangle(
                    0,
                    HealthSphere.Height - HealthSphereAdjustedHeight, //height to start drawing texture based on player health
                    HealthSphere.Width,
                    HealthSphereAdjustedHeight),
                ColorValue(HealthColorAngle)
                );

            //player stamina sphere
            int StaminaSphereAdjustedHeight = (int)(HealthSphere.Height * Player.PercentStamina);
            StaminaColorAngle = NextColorAngle(Player.PercentStamina, StaminaColorAngle);
            GameRef.spriteBatch.Draw(
                StaminaSphere,
                new Vector2(
                    GameRef.ScreenRectangle.Width - StaminaSphere.Width,
                    GameRef.ScreenRectangle.Height - StaminaSphereAdjustedHeight), //shift image down based on player stamina
                new Rectangle(
                    0,
                    StaminaSphere.Height - StaminaSphereAdjustedHeight, //height to start drawing texture based on player stamina
                    StaminaSphere.Width,
                    StaminaSphereAdjustedHeight
                    ),
                ColorValue(StaminaColorAngle)
                );

            //overlay - must be on top of hp/stamina spheres
            GameRef.spriteBatch.Draw(Overlay, new Vector2(0, 0), Color.White);


            //draws hearts
            for (int i = 0; i < Player.lives; ++i)
            {
                GameRef.spriteBatch.Draw(
                    lifeHeart,
                    new Vector2(
                        125 + (i * 28),
                        575),
                    Color.White);
            }


            /*
            for (int i = 0; i < Player.lives; ++i)
            {
                GameRef.spriteBatch.Draw(lifeHeart, new Vector2(GameRef.ScreenRectangle.Width - 28, GameRef.ScreenRectangle.Height - 28 - (i * 28)), Color.White);
            }
             * */

            foreach (Notification note in notifications)
                note.Draw(GameRef.spriteBatch, notificationFont);

            GameRef.spriteBatch.End();
        }
    }
}
