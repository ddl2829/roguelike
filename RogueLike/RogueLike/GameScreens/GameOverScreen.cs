﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameLibrary.Controls;

namespace RogueLike.GameScreens
{
    public class GameOverScreen : BaseGameScreen
    {
        public bool win = false;
        Label winLoss;
        LinkLabel newGameLabel;
        LinkLabel quitLabel;

        public GameOverScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            Label gameTitle = new Label();
            gameTitle.SpriteFont = GameRef.Content.Load<SpriteFont>(@"Fonts\TitleFont");
            gameTitle.Text = "Game Over";
            gameTitle.Position = new Vector2((GameRef.ScreenRectangle.Width - gameTitle.SpriteFont.MeasureString(gameTitle.Text).X) / 2, GameRef.ScreenRectangle.Height / 3 - 50);
            ControlManager.Add(gameTitle);

            winLoss = new Label();
            winLoss.SpriteFont = GameRef.Content.Load<SpriteFont>(@"Fonts\TitleFont");
            winLoss.Text = "";
            winLoss.Position = new Vector2((GameRef.ScreenRectangle.Width - winLoss.SpriteFont.MeasureString(winLoss.Text).X) / 2, GameRef.ScreenRectangle.Height / 3 - 20);
            ControlManager.Add(winLoss);

            /* New Game not functional */
            newGameLabel = new LinkLabel();
            newGameLabel.Text = "New Game";
            newGameLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - newGameLabel.SpriteFont.MeasureString(newGameLabel.Text).X) / 2, GameRef.ScreenRectangle.Height / 2);
            newGameLabel.Color = Color.White;
            newGameLabel.HasFocus = true;
            //newGameLabel.Selected += new EventHandler(newGameLabel_Selected);
            ControlManager.Add(newGameLabel);
            
            quitLabel = new LinkLabel();
            quitLabel.Text = "Quit";
            quitLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - quitLabel.SpriteFont.MeasureString(quitLabel.Text).X) / 2, GameRef.ScreenRectangle.Height / 2 + 30);
            quitLabel.Color = Color.White;
            quitLabel.HasFocus = false;
            quitLabel.Selected += new EventHandler(quitLabel_Selected);
            ControlManager.Add(quitLabel);
        }

        void quitLabel_Selected(object sender, EventArgs e)
        {
            GameRef.Exit();
        }

        void newGameLabel_Selected(object sender, EventArgs e)
        {
            ActionScreen ActionScreen = new ActionScreen(GameRef, StateManager);
            ActionScreen.Initialize();
            GameRef.ActionScreen = new ActionScreen(GameRef, StateManager);
            GameRef.ActionScreen.Initialize();
            StateManager.ChangeState(GameRef.ActionScreen);
            GameRef.ActionScreen = ActionScreen;
        }

        public override void Update(GameTime gameTime)
        {
            winLoss.Text = win ? "You defeated the boss on this floor" : "You were slaughtered";
            winLoss.Position = new Vector2((GameRef.ScreenRectangle.Width - winLoss.SpriteFont.MeasureString(winLoss.Text).X) / 2, GameRef.ScreenRectangle.Height / 3 - 20);

            ControlManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.spriteBatch.Begin();
            ControlManager.Draw(GameRef.spriteBatch);
            GameRef.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
