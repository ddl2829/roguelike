﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using GameLibrary.Controls;
using GameLibrary;

namespace RogueLike.GameScreens
{
    public abstract class BaseGameScreen : GameState
    {
        protected Game1 GameRef;
        protected ControlManager ControlManager;

        public BaseGameScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {
            GameRef = game;
        }

        protected override void LoadContent()
        {
            ContentManager Content = Game.Content;
            SpriteFont menuFont = Content.Load<SpriteFont>(@"Fonts\ControlFont");
            ControlManager = new ControlManager(menuFont);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}