﻿using GameLibrary;
using GameLibrary.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using Microsoft.Xna.Framework.Graphics;
using RogueLike.Components;

namespace RogueLike.GameScreens
{
    public class TitleScreen : BaseGameScreen
    {
        LinkLabel startLabel;
        LinkLabel quitLabel;

        public TitleScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            Label gameTitle = new Label();
            gameTitle.SpriteFont = GameRef.Content.Load<SpriteFont>(@"Fonts\TitleFont");
            gameTitle.Text = "Dungeon Crawl";
            gameTitle.Position = new Vector2((GameRef.ScreenRectangle.Width - gameTitle.SpriteFont.MeasureString(gameTitle.Text).X) / 2, GameRef.ScreenRectangle.Height / 3 - 20);
            ControlManager.Add(gameTitle);

            Label name1 = new Label();
            name1.Text = "Dale Smith";
            name1.SpriteFont = GameRef.Content.Load<SpriteFont>(@"Fonts\SmallFont");
            name1.Position = new Vector2(0,0);
            ControlManager.Add(name1);

            Label name2 = new Label();
            name2.Text = "Gregory Linscott";
            name2.SpriteFont = GameRef.Content.Load<SpriteFont>(@"Fonts\SmallFont");
            name2.Position = new Vector2(0, 20);
            ControlManager.Add(name2);

            Label name3 = new Label();
            name3.Text = "Steven Liu";
            name3.SpriteFont = GameRef.Content.Load<SpriteFont>(@"Fonts\SmallFont");
            name3.Position = new Vector2(0, 40);
            ControlManager.Add(name3);

            startLabel = new LinkLabel();
            startLabel.Text = "New Game";
            startLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - startLabel.SpriteFont.MeasureString(startLabel.Text).X) / 2, 2 * GameRef.ScreenRectangle.Height / 3 + 20);
            startLabel.Color = Color.White;
            startLabel.HasFocus = true;
            startLabel.Selected += new EventHandler(startLabel_Selected);
            ControlManager.Add(startLabel);

            quitLabel = new LinkLabel();
            quitLabel.Text = "Exit Game";
            quitLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - quitLabel.SpriteFont.MeasureString(quitLabel.Text).X) / 2, 2 * GameRef.ScreenRectangle.Height / 3 + 50);
            quitLabel.Color = Color.White;
            quitLabel.HasFocus = false;
            quitLabel.Selected += new EventHandler(quitLabel_Selected);
            ControlManager.Add(quitLabel);
        }

        void quitLabel_Selected(object sender, EventArgs e)
        {
            GameRef.Exit();
        }

        void startLabel_Selected(object sender, EventArgs e)
        {
            GameRef.ActionScreen.AcceptInput = true;
            GameRef.ActionScreen.notifications.Add(new Notification("Move with W A S D or by clicking the mouse", 200, 500, 3.0f));
            StateManager.PopState();
        }

        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.spriteBatch.Begin();
            ControlManager.Draw(GameRef.spriteBatch);
            GameRef.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
