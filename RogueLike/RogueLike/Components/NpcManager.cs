using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Roguelike.Components;
using RogueLike.Components.Helpers;
using RogueLike.Components.NpcClasses;

namespace RogueLike.Components
{
    public class NpcManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public event EventHandler NpcDeath;

        public Dictionary<int, NpcBase> Npcs;

        World worldRef;
        Texture2D healthBar;        
        SpriteBatch batch;

        public int index = 0;
        
        public NpcManager(Game1 game, World world)
            : base(game)
        {
            worldRef = world;
            Npcs = new Dictionary<int, NpcBase>();
        }

        public void SerilizeNpc(NpcAttributes attributes, string fileName)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(@"Content\Npcs\" + fileName + ".xml", settings))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(NpcAttributes));
                serializer.Serialize(writer, attributes);
            }
        }

        public NpcAttributes DeserializeNpc(string fileName)
        {
            using (XmlReader reader = XmlReader.Create(@"Content\Npcs\" + fileName + ".xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(NpcAttributes));
                return (NpcAttributes)serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }
        
        public void Clear()
        {
            foreach (NpcBase npc in Npcs.Values)
            {
                worldRef.theWorld.Remove(npc.Controller.BepuController);
            }
            Npcs.Clear();
        }

        public void Add(NpcBase npc)
        {
            Npcs[index++] = npc;
            npc.OnDeath += new EventHandler(npc_OnDeath);
            worldRef.Add(npc.Controller.BepuController);
        }

        
        void npc_OnDeath(object sender, EventArgs e)
        {
            NpcDeath(sender, null);
        }

        public Dictionary<int, NpcBase> NpcList
        {
            get { return Npcs; }
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            batch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            healthBar = Game.Content.Load<Texture2D>(@"Textures\UI\HP");
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            foreach (NpcBase npc in Npcs.Values)
            {
                npc.Update(gameTime);
            }
            base.Update(gameTime);
        }

        float healthBarWidth = 100;
        public override void Draw(GameTime gameTime)
        {
            foreach (NpcBase npc in Npcs.Values)
            {
                GraphicsDevice.BlendState = BlendState.Opaque;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
                npc.Draw(gameTime);
                Vector3 position = GraphicsDevice.Viewport.Project(npc.Controller.Position + new Vector3(0, 1, 0), RogueCamera.Projection, RogueCamera.View, Matrix.Identity);
                if (npc.hitsubbed)
                {
                    batch.Begin();
                    batch.Draw(healthBar, new Rectangle((int)(position.X - 2 - healthBarWidth / 2), (int)position.Y - 2, 104, 14), Color.Black);
                    batch.Draw(healthBar, new Rectangle((int)(position.X - healthBarWidth / 2), (int)position.Y, (int)(100 * npc.percentHealth), 10), Color.Red);
                    batch.End();
                }
#if DEBUG
                BoundingSphereRenderer.Render(npc.hitArea, GraphicsDevice, RogueCamera.View, RogueCamera.Projection, Color.Yellow);
#endif
            }

            base.Draw(gameTime);
        }
    }
}
