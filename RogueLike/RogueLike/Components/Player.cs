using System;
using System.Diagnostics;
using GameLibrary;
using GameLibrary.Animation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Roguelike.Components;
using RogueLike.Components.Helpers;
using RogueLike.Components.NpcClasses;

namespace RogueLike.Components
{    
    public class Player : Microsoft.Xna.Framework.DrawableGameComponent
    {
        //Player Events
        public event EventHandler OnAttack;
        public event EventHandler OnDeath;
        public event EventHandler OnAction;
        public event EventHandler OnNoLivesLeft;

        //Player Base Data
        Model model;
        string PlayerClass;
        public int lives = 3;
        public int Strength = 1;
        public int health = 100;
        public int maxHealth = 100;
        public float PercentHealth
        {
            get { return health / (float)maxHealth; }
        }
        public float stamina = 3000;
        public float maxStamina = 3000;
        public float PercentStamina
        {
            get { return stamina / (float)maxStamina; }
        }
        public RogueCharacterController Controller;
        public float WalkSpeed = 1.5f;
        public float Rotation = 0.0f;
        public float RotationSpeed = 1.0f;
        public Vector3 PlayerRespawnPosition = new Vector3(0, 2, 0);
        public BoundingSphere hitArea;
        public bool hasKey = false;
        public Vector2 screenSize = Vector2.Zero;
        bool attacked = false;
        bool attackedTwice = false;
        
        //Accept Input
        bool accept_input = true;
        public bool AcceptInput
        {
            get { return accept_input; }
            set { accept_input = value; }
        }
        public bool accept_input_private = true;

        //Animation Data
        RogueAnimator Animator;
        public AnimationState AnimationState;
        AnimationState LastAnimationState;
        //End Animation Data
          
        public Player(Game game, string playerClass)
            : base(game)
        {
            PlayerClass = playerClass;
            Controller = new RogueCharacterController(new Vector3(0, 2, 0), 2.0f, 0.4f, 20.0f);
            Controller.Rotate(90.0f);
            Controller.SetSpeed(WalkSpeed);
            Controller.BepuController.Body.CollisionInformation.Entity.Tag = this;
            AnimationState = AnimationState.Idle;
            hitArea = new BoundingSphere(Controller.Position + Controller.Forward, 0.8f);
        }

        /// <summary>
        /// Reset the player's position in the world
        /// </summary>
        /// <param name="position"></param>
        public void Respawn(Vector3 position)
        {
            Controller = new RogueCharacterController(position, 2.0f, 0.4f, 20.0f);
            Controller.Rotate(90.0f);
            Controller.SetSpeed(WalkSpeed);
            Controller.BepuController.Body.CollisionInformation.Entity.Tag = this;
            health = maxHealth;
            lives -= 1;
            if (lives == 0 && OnNoLivesLeft != null)
                OnNoLivesLeft(this, null);
        }

        /// <summary>
        /// Sets the location where respawn places you
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        public void setRespawn(float x, float z)
        {
            PlayerRespawnPosition = new Vector3(x, 2, z);
        }

        protected override void LoadContent()
        {
            model = Game.Content.Load<Model>(String.Format(@"Models\Characters\{0}", PlayerClass));
            Animator = new RogueAnimator(model.Tag as SkinningData);
            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        
        public override void Update(GameTime gameTime)
        {            
            if(AcceptInput && accept_input_private)
                HandleInput(gameTime);

            if (AnimationState == AnimationState.Dying)
            {
                if (Animator.AnimationTime > TimeSpan.FromSeconds(1.0))
                {
                    if (OnDeath != null)
                        OnDeath(this, null);
                }
            }

            TimeSpan firstAttack = TimeSpan.FromSeconds(0.6f);
            TimeSpan secondAttack = TimeSpan.FromSeconds(1.0f);

            if (Animator.AnimationTime < firstAttack)
            {
                attacked = false;
                attackedTwice = false;
            }
            if (AnimationState == AnimationState.Attacking && Animator.AnimationTime >= firstAttack)
            {
                if (!attacked)
                {
                    attacked = true;
                    if (OnAttack != null)
                        OnAttack(this, null);
                }
                if (Animator.AnimationTime >= secondAttack && !attackedTwice)
                {
                    attackedTwice = true;
                    if (OnAttack != null)
                        OnAttack(this, null);
                }
            }

            if (AnimationState != LastAnimationState)
            {
                switch (AnimationState)
                {
                    case AnimationState.Idle:
                        Animator.PlayAnimation("Idle", 0.73f);
                        break;
                    case AnimationState.Walking:
                        Animator.PlayAnimation("Walk", 0.73f);
                        break;
                    case AnimationState.Jumping:
                        break;
                    case AnimationState.Attacking:
                        Animator.PlayAnimation("Attack", 1.2f);
                        break;
                    case AnimationState.Dying:
                        Animator.PlayAnimation("Die", 1.0f);
                        break;
                }
            }

            LastAnimationState = AnimationState;
            
            Animator.Update(gameTime);

            hitArea.Center = Controller.Position + Controller.Forward * 0.7f - new Vector3(0, 1, 0);

            base.Update(gameTime);
        }

        /// <summary>
        /// Response when an NPC attacks the player
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Npc_Attacked(object sender, EventArgs e)
        {
            NpcBase enemy = (NpcBase)sender;
            Vector3 awayFromEnemy = Vector3.Normalize(Controller.Position - enemy.Controller.Position);
            Vector3 thrust = awayFromEnemy * 200;
            Controller.Thrust(thrust);

            health -= enemy.Strength;
            if (health <= 0)
            {
                AnimationState = AnimationState.Dying;
                accept_input_private = false;
            }
        }

        /// <summary>
        /// Handles the player's input
        /// </summary>
        /// <param name="gameTime"></param>
        private void HandleInput(GameTime gameTime)
        {
            AnimationState = AnimationState.Idle;
            Vector3 totalMovement = Vector3.Zero;
            if (InputHandler.GamePadConnected())
            {
                totalMovement = HandleGamePadInput();
            }
            else
            {
                totalMovement = HandleKeyboardMouseInput(gameTime);
            }
            Controller.Move(totalMovement);
        }

        /// <summary>
        /// Handles input from the keyboard and mouse
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        private Vector3 HandleKeyboardMouseInput(GameTime gameTime)
        {
            Vector3 totalMovement = Vector3.Zero;            
            Controller.SetSpeed(WalkSpeed);
            if (InputHandler.KeyDown(Keys.LeftShift))
            {
                if (stamina > 0)
                {
                    Controller.SetSpeed(WalkSpeed * 2.0f);
                    stamina -= gameTime.ElapsedGameTime.Milliseconds / 2;
                }
            }
            else
            {
                if (stamina < maxStamina)
                {
                    stamina += gameTime.ElapsedGameTime.Milliseconds / 3;
                }
            }

            if (InputHandler.KeyPressed(Keys.LeftControl))
            {
                if(OnAction != null)
                    OnAction(this, null);
            }

            if (InputHandler.MouseRightClicked())
            {
                Controller.BepuController.Jump();
            }

            if (InputHandler.KeyPressed(Keys.P))
            {
                Debug.Print("{0}, {1}, {2}", Controller.Position.X, Controller.Position.Y, Controller.Position.Z);
            }

            if (InputHandler.MouseLeftButtonDown())
            {
                Vector2 ScreenTarget = InputHandler.MousePosition();
                ScreenTarget -= screenSize/2;
                ScreenTarget.Y = -ScreenTarget.Y;
                ScreenTarget.Normalize();
                Controller.RotateAbsolute(MathHelper.ToDegrees((float)Math.Atan2(ScreenTarget.X, ScreenTarget.Y)) + 90.0f);
                totalMovement += new Vector3(ScreenTarget.Y, 0, ScreenTarget.X);
                AnimationState = AnimationState.Walking;
            }

            if (InputHandler.KeyDown(Keys.W))
            {
                totalMovement += Controller.Forward;
                AnimationState = AnimationState.Walking;
            }
            if (InputHandler.KeyDown(Keys.S))
            {
                totalMovement -= Controller.Forward;
                AnimationState = AnimationState.Walking;
            }
            if (InputHandler.KeyDown(Keys.Q))
            {
                totalMovement += Controller.Left;
                AnimationState = AnimationState.Walking;
            }
            if (InputHandler.KeyDown(Keys.E))
            {
                totalMovement -= Controller.Left;
                AnimationState = AnimationState.Walking;
            }
            if (InputHandler.KeyDown(Keys.A))
            {
                Controller.Rotate(-RotationSpeed);
                AnimationState = AnimationState.Walking;
            }
            if (InputHandler.KeyDown(Keys.D))
            {
                Controller.Rotate(RotationSpeed);
                AnimationState = AnimationState.Walking;
            }
            if (InputHandler.KeyDown(Keys.Space))
            {
                AnimationState = AnimationState.Attacking;
                Controller.SetSpeed(WalkSpeed / 4);
            }
            return totalMovement;
        }

        /// <summary>
        /// Handles input from the game pad if the inputhandler indicates an xbox controller is connected
        /// </summary>
        /// <returns></returns>
        private Vector3 HandleGamePadInput()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Draws the player
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
#if DEBUG
            BoundingSphereRenderer.Render(hitArea, GraphicsDevice, RogueCamera.View, RogueCamera.Projection, Color.White);
#endif
            Matrix[] bones = Animator.SkinTransforms;
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (SkinnedEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.SetBoneTransforms(bones);
                    effect.World = Matrix.CreateScale(0.1f) * Matrix.CreateRotationY(-Controller.Rotation + MathHelper.ToRadians(180.0f)) * Matrix.CreateTranslation(Controller.Position - new Vector3(0,1,0));
                    effect.View = RogueCamera.View;
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90.0f), GraphicsDevice.Viewport.AspectRatio, 0.01f, 10000.0f);
                }
                mesh.Draw();
            }
            base.Draw(gameTime);
        }

    }
}
