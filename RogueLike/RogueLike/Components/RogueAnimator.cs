﻿using System;
using System.Linq;
using GameLibrary.Animation;
using Microsoft.Xna.Framework;

namespace RogueLike.Components
{
    public enum AnimationState { Walking, Jumping, Idle, Attacking, Sneaking, Dying, Other };

    public class RogueAnimator
    {
        AnimationPlayer _player;
        AnimationClip _clip;
        SkinningData _skin;
        TimeSpan _duration = TimeSpan.Zero;
        
        public RogueAnimator(SkinningData SkinningData)
        {
            _skin = SkinningData;
            _player = new AnimationPlayer(_skin);
        }

        public TimeSpan AnimationTime
        {
            get { return _player.CurrentTime; }
        }

        public TimeSpan AnimationDuration
        {
            get { return _duration; }
        }

        public Matrix[] SkinTransforms
        {
            get { return _player.GetSkinTransforms(); }
        }

        public void Update(GameTime gameTime)
        {
            if (_duration != TimeSpan.Zero)
            {
                if (_player.CurrentTime > _duration)
                {
                    _player.StartClip(_clip);
                }
            }
            _player.Update(gameTime.ElapsedGameTime, true, Matrix.Identity);
        }

        public void PlayAnimation(string AnimationName, float AnimationDuration)
        {
            if (_skin.AnimationClips.Keys.Contains<string>(AnimationName))
            {
                _duration = TimeSpan.FromSeconds(AnimationDuration);
                _clip = _skin.AnimationClips[AnimationName];
                _player.StartClip(_clip);
                return;
            }
            throw new Exception("Invalid Animation Name");
        }

    }
}
