﻿
namespace RogueLike.Components
{
    public class NpcAttributes
    {
        public string Model;
        public int Max_Health;
        public int Strength;
        public float AttackSpeed;

        public NpcAttributes()
        {

        }

        /// <summary>
        /// Initialize a new set of npc attributes
        /// </summary>
        /// <param name="m">Model Name</param>
        /// <param name="h">Max HP</param>
        /// <param name="s">Strength</param>
        /// <param name="a">Attack Speed in Seconds</param>
        public NpcAttributes(string m, int h, int s, float a)
        {
            Model = m;
            Max_Health = h;
            Strength = s;
            AttackSpeed = a;
        }
    }
}
