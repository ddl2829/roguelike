﻿using System;
using GameLibrary.Animation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Roguelike.Components;

namespace RogueLike.Components.NpcClasses
{
    public enum NpcType { Neutral, Friendly, Hostile };
    public class NpcBase
    {
        public event EventHandler OnDeath;
        public event EventHandler OnAttack;

        public NpcType Type = NpcType.Hostile;
        public Model Model;
        public Texture2D Texture;
        public int Strength = 1;
        int health = 10;
        int maxhealth = 10;
        public float percentHealth
        {
            get { return health / (float)maxhealth; }
        }
        public bool AttackSubscribed
        {
            get { return OnAttack != null; }
        }
        public BoundingSphere hitArea;
        public RogueCharacterController Controller;
        public Effect pointLight;
        public bool hitsubbed = false;

        //Animation Data
        RogueAnimator Animator;
        AnimationState AnimationState = AnimationState.Idle;
        AnimationState LastAnimationState = AnimationState.Other;
        //End Animation Data

        TimeSpan counter = TimeSpan.Zero;
        TimeSpan attackSpeed = TimeSpan.FromSeconds(0.9f);

        public NpcBase(Vector3 _position, Model _model)
        {
            Controller = new RogueCharacterController(_position, 2.0f, .4f, 20.0f);
            Animator = new RogueAnimator(_model.Tag as SkinningData);

            Controller.BepuController.Body.CollisionInformation.Entity.Tag = this;

            hitArea = new BoundingSphere(Controller.Position + Controller.Forward, 0.3f);

            AnimationState = AnimationState.Idle;

            Texture = ((SkinnedEffect)_model.Meshes[0].Effects[0]).Texture;
            Model = _model;

            Controller.SetSpeed(1.5f);
        }

        public void TrackTarget(Vector3 targetPos)
        {
            Vector3 toTarget = targetPos - Controller.Position;
            if (targetPos == Vector3.Zero || hitArea.Intersects(new BoundingSphere(targetPos, 0.3f)))
            {
                Controller.Move(Vector3.Zero);
                return;
            }
            else
            {
                Controller.Move(toTarget);
            }
            Controller.RotateAbsolute(-MathHelper.ToDegrees((float)Math.Atan2(toTarget.X, toTarget.Z)) + 180.0f);
        }


        public void SetEffect(Effect e)
        {
            pointLight = e;
        }

        public void Update(GameTime gameTime)
        {

            hitArea.Center = Controller.Position + Controller.Forward * .2f - new Vector3(0, 1, 0);

            if (AnimationState != AnimationState.Attacking || (AnimationState == AnimationState.Attacking && Animator.AnimationTime > attackSpeed))
            {
                if (Controller.BepuController.HorizontalMotionConstraint.MovementDirection == Vector2.Zero)
                {
                    AnimationState = AnimationState.Idle;
                }
                else
                {
                    AnimationState = AnimationState.Walking;
                }
            }

            counter += gameTime.ElapsedGameTime;

            if (OnAttack != null)
            {
                if (AnimationState != AnimationState.Attacking)
                {
                    if (counter > attackSpeed)
                    {
                        counter = TimeSpan.Zero;
                        AnimationState = AnimationState.Attacking;
                        OnAttack(this, null);
                    }
                }
            }

            if (AnimationState != LastAnimationState)
            {
                switch (AnimationState)
                {
                    case AnimationState.Idle:
                        Animator.PlayAnimation("Idle", 0.73f);
                        break;
                    case AnimationState.Walking:
                        Animator.PlayAnimation("Walk", 1.0f);
                        break;
                    case AnimationState.Attacking:
                        Animator.PlayAnimation("Attack", 2.0f);
                        break;
                }

            }

            LastAnimationState = AnimationState;

            Animator.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            Matrix[] bones = Animator.SkinTransforms;

            pointLight.CurrentTechnique = pointLight.Techniques["PointLight_Animated"];
            pointLight.Parameters["View"].SetValue(RogueCamera.View);
            pointLight.Parameters["Projection"].SetValue(RogueCamera.Projection);
            pointLight.Parameters["Bones"].SetValue(bones);
            foreach (ModelMesh mesh in Model.Meshes)
            {
                pointLight.Parameters["ModelTexture"].SetValue(Texture);
                pointLight.Parameters["World"].SetValue(Matrix.CreateScale(0.15f) * Matrix.CreateRotationY(-Controller.Rotation + MathHelper.ToRadians(180.0f)) * Matrix.CreateTranslation(Controller.Position - new Vector3(0, .8f, 0)));
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = pointLight;
                }
                foreach (Effect effect in mesh.Effects)
                {
                    foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                    {
                        pass.Apply();
                    }
                }
                mesh.Draw();
            }
        }


        internal void SubscribeToHit(Player Player)
        {
            if (!hitsubbed)
            {
                hitsubbed = true;
                Player.OnAttack += new EventHandler(Player_onAttack);
            }
        }

        public void Damage(int amt)
        {
            health -= amt;
            if (health <= 0)
                OnDeath(this, null);
        }

        public void Player_onAttack(object sender, EventArgs e)
        {
            Player player = ((Player)sender);
            health -= player.Strength;

            Vector3 awayFromPlayer = Vector3.Normalize(Controller.Position - player.Controller.Position);
            Vector3 thrust = awayFromPlayer * 200;
            Controller.Thrust(thrust);
            if (health <= 0)
                OnDeath(this, null);
        }

        internal void RemoveHitSubscription(Player Player)
        {
            if (hitsubbed)
            {
                Player.OnAttack -= Player_onAttack;
                hitsubbed = false;
            }
        }

        internal void SetAttributes(NpcAttributes attributes)
        {
            Strength = attributes.Strength;
            attackSpeed = TimeSpan.FromSeconds(attributes.AttackSpeed);
            health = attributes.Max_Health;
            maxhealth = attributes.Max_Health;
        }
    }
}
