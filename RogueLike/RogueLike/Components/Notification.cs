﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RogueLike.Components
{
    public class Notification
    {
        string text = "";
        Vector2 position = Vector2.Zero;
        TimeSpan displayTime = TimeSpan.Zero;
        TimeSpan timeShown = TimeSpan.Zero;

        public bool Expired
        {
            get { return timeShown > displayTime; }
        }

        public Notification(string text, int x, int y, float seconds)
        {
            this.text = text;
            this.position = new Vector2(x, y);
            this.displayTime = TimeSpan.FromSeconds(seconds);
        }

        public void Update(GameTime gameTime)
        {
            timeShown += gameTime.ElapsedGameTime;
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont)
        {
            spriteBatch.DrawString(spriteFont, text, position, Color.White);
        }
    }
}
