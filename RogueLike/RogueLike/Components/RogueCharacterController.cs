﻿using System;
using Microsoft.Xna.Framework;
using RogueLike.Components.CharacterControls;

namespace RogueLike.Components
{
    public class RogueCharacterController
    {
        CharacterController Controller;

        public Vector3 Position
        {
            get { return Controller.Body.Position; }
        }

        public CharacterController BepuController
        {
            get { return Controller; }
        }

        public float Rotation
        {
            get { return totalRotation; }
            set { totalRotation = value; }
        }

        public Vector3 Forward
        {
            get { return Controller.Body.OrientationMatrix.Forward; }
        }

        public Vector3 Left
        {
            get { return Controller.Body.OrientationMatrix.Left; }
        }

        public RogueCharacterController(Vector3 Position, float Height, float Radius, float Mass)
        {
            Controller = new CharacterController(Position, Height, 0, Radius, Mass);
        }

        float totalRotation = 0;
        public void Rotate(float amount)
        {            
            float radians = MathHelper.ToRadians(amount);
            totalRotation += radians;
            float cosRot = (float)Math.Cos(totalRotation);
            float sinRot = (float)Math.Sin(totalRotation);
            Controller.Body.OrientationMatrix = new BEPUphysics.MathExtensions.Matrix3X3(cosRot, 0f, sinRot, 0f, 1f, 0f, -sinRot, 0, cosRot);
        }

        public void RotateAbsolute(float amount)
        {
            float radians = MathHelper.ToRadians(amount);
            totalRotation = radians;
            float cosRot = (float)Math.Cos(totalRotation);
            float sinRot = (float)Math.Sin(totalRotation);
            Controller.Body.OrientationMatrix = new BEPUphysics.MathExtensions.Matrix3X3(cosRot, 0f, sinRot, 0f, 1f, 0f, -sinRot, 0, cosRot);
        }

        public void Move(Vector3 Movement)
        {
            if (Movement != Vector3.Zero)
                Controller.HorizontalMotionConstraint.MovementDirection = Vector2.Normalize(new Vector2(Movement.X, Movement.Z));
            else
                Controller.HorizontalMotionConstraint.MovementDirection = Vector2.Zero;
        }

        public void Thrust(Vector3 Movement)
        {
            Controller.Body.ApplyLinearImpulse(ref Movement);
        }

        public void SetSpeed(float speed)
        {
            Controller.HorizontalMotionConstraint.Speed = speed;
        }
    }
}
