﻿using Microsoft.Xna.Framework;
using RogueLike.Components;

namespace Roguelike.Components
{
    public class RogueCamera : Microsoft.Xna.Framework.GameComponent
    {
        public static Matrix View;
        public static Matrix Projection;

        private Vector3 Position;
        private Player Player;

        public Vector3 height = new Vector3(0, 4, 0);

        public RogueCamera(Game game, Player player)
            : base(game)
        {
            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90.0f), game.GraphicsDevice.Viewport.AspectRatio, 0.01f, 10000.0f);
            Player = player;
            Position = player.Controller.Position - player.Controller.Forward * 3 + height;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            Position = Player.Controller.Position + new Vector3(-3, 0, 0) + height;
            View = Matrix.CreateLookAt(Position, Player.Controller.Position, Vector3.Up);
            base.Update(gameTime);
        }
    }
}
