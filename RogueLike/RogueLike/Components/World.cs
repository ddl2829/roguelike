using System;
using System.Collections.Generic;
using BEPUphysics;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.MathExtensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Roguelike.Components;
using RogueLike.Components.NpcClasses;
using XRogueLibrary;


namespace RogueLike.Components
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class World : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public Space theWorld;
        LevelFactory LevelF;
        public Level currentLevel;
                
        public Vector3 pointLightPosition = new Vector3(-0.1f, -1.9f, 7.4f);
        public Effect pointLight;
        float att = 5.0f;
        float falloff = 2.0f;

        public Dictionary<int, StaticMesh> doodadMeshes;

        public NpcManager Npcs;

        public event EventHandler ActorDeath;

        public World(Game1 game)
            : base(game)
        {
        }

        public Dictionary<int, NpcBase> NpcList
        {
            get { return Npcs.NpcList; }
        }

        public void Add(ISpaceObject spaceObject)
        {
            theWorld.Add(spaceObject);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            LevelF = new LevelFactory(Game.Content);
            LoadLevel("Level");

            theWorld = new Space();
            theWorld.ForceUpdater.Gravity = new Vector3(0, -9.81f, 0);

            doodadMeshes = new Dictionary<int, StaticMesh>();

            GetLevelPhysicsMeshes();

            Npcs = new NpcManager((Game1)Game, this);
            Game.Components.Add(Npcs);
            Npcs.NpcDeath += new EventHandler(Npcs_NpcDeath);

            base.Initialize();
        }
        
        void Npcs_NpcDeath(object sender, EventArgs e)
        {
            ActorDeath(sender, null);     
        }

        private void LoadLevel(string levelName)
        {
            currentLevel = LevelF.LoadLevel(levelName);
            //currentLevel = LevelF.Generate();
        }

        public void RemoveDoodad(int index)
        {
            theWorld.Remove(doodadMeshes[index]);
            doodadMeshes[index] = null;
            currentLevel.GetDoodads()[index] = null;
        }

        
        private void GetLevelPhysicsMeshes()
        {
            foreach (StaticMesh mesh in LevelF.GetLevelPhysicsMeshes(currentLevel))
            {
                mesh.Tag = this;
                theWorld.Add(mesh);
            }
            foreach (int index in currentLevel.GetDoodads().Keys)
            {
                Vector3[] vertices;
                int[] indices;
                TriangleMesh.GetVerticesAndIndicesFromModel(currentLevel.GetDoodads()[index], out vertices, out indices);
                var mesh = new StaticMesh(vertices, indices, new AffineTransform(new Vector3(currentLevel.doodads[index].Scale), Quaternion.CreateFromRotationMatrix(Matrix.CreateRotationY(MathHelper.ToRadians(currentLevel.doodads[index].Rotation))), currentLevel.doodads[index].Position + new Vector3(0, 2, 0)));
                doodadMeshes.Add(index, mesh);
                theWorld.Add(mesh);
            }
        }
        
        protected override void LoadContent()
        {
            base.LoadContent();
                        
            pointLight = Game.Content.Load<Effect>(@"Effects\PointLight");

            pointLight.Parameters["Attenuation"].SetValue(att);
            pointLight.Parameters["Falloff"].SetValue(falloff);
            pointLight.Parameters["LightPosition"].SetValue(pointLightPosition);
            
            foreach (NpcData npc in currentLevel.npcs)
            {
                NpcAttributes attributes = Npcs.DeserializeNpc(npc.Data);
                NpcBase newnpc = new NpcBase(npc.Position, Game.Content.Load<Model>(@"Models\Characters\"+attributes.Model));
                newnpc.SetAttributes(attributes);
                newnpc.SetEffect(pointLight);
                Npcs.Add(newnpc);
            }
        }

        private StaticMesh CreateStaticMeshFromModel(Model model)
        {
            Vector3[] vertices;
            int[] indices;
            TriangleMesh.GetVerticesAndIndicesFromModel(model, out vertices, out indices);
            var mesh = new StaticMesh(vertices, indices, new AffineTransform(new Vector3(0.03f), new Quaternion(), new Vector3(0, 1, 0)));
            mesh.Tag = this;
            return mesh;
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            theWorld.Update();
            Npcs.Update(gameTime);
            
            base.Update(gameTime);
        }
                
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            RasterizerState rs = new RasterizerState();
            rs.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rs;

            pointLight.Parameters["View"].SetValue(RogueCamera.View);
            pointLight.Parameters["Projection"].SetValue(RogueCamera.Projection);

            currentLevel.Draw(pointLight);
            
            Npcs.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
