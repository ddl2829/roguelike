﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.MathExtensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XRogueLibrary;

namespace RogueLike.Components
{
    public class LevelFactory
    {
        ContentManager Content;

        public LevelFactory(ContentManager c)
        {
            Content = c;
        }
        
        public DungeonRoom GetRoomDataFromFile(string filePath)
        {
            return Content.Load<DungeonRoom>(@"Dungeons\Rooms\"+filePath);
        }

        public void SerializeLevel(Level level, string fileName)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(@"Content\Dungeons\Levels\"+fileName+".xml", settings))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Level));
                serializer.Serialize(writer, level);
            }            
        }

        public Level DeserializeLevel(string fileName)
        {
            using (XmlReader reader = XmlReader.Create(@"Content\Dungeons\Levels\" + fileName + ".xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Level));
                return (Level)serializer.Deserialize(reader);
            }
        }

        public void GetLevelModels(Level level)
        {
            Dictionary<int, Model> dict = new Dictionary<int, Model>();
            int accum = 0;
            foreach (RoomData data in level.rooms)
            {
                dict.Add(accum++, Content.Load<Model>(@"Models\Rooms\"+GetRoomDataFromFile(data.RoomDataPath).Mesh));
            }
            level.SetModels(dict);
            Dictionary<int, Model> dads = new Dictionary<int, Model>();
            accum = 0;
            foreach (Doodad dad in level.doodads)
            {
                dads.Add(accum++, Content.Load<Model>(@"Models\Doodads\"+dad.Model));
            }
            level.SetDoodadModels(dads);
        }

        public void GetModelTextures(Level level)
        {
            Dictionary<int, Texture2D> dict = new Dictionary<int, Texture2D>();
            foreach (int key in level.GetModels().Keys)
            {
                dict.Add(key, ((BasicEffect)level.GetModels()[key].Meshes[0].Effects[0]).Texture);
            }
            level.SetTextures(dict);
            Dictionary<int, Texture2D> dads = new Dictionary<int, Texture2D>();
            foreach (int key in level.GetDoodads().Keys)
            {
                if (level.doodads[key].Texture == "")
                {
                    dads.Add(key, ((BasicEffect)level.GetDoodads()[key].Meshes[0].Effects[0]).Texture);
                }
                else
                {
                    dads.Add(key, Content.Load<Texture2D>(@"Textures\ModelTextures\"+level.doodads[key].Texture));
                }
            }
            level.SetDoodadTextures(dads);
        }

        public Dictionary<int, float> Rotations = new Dictionary<int, float>();

        public void GetLevelRoomTransforms(Level level)
        {
            Rotations.Clear();
            Dictionary<int, Matrix> dict = new Dictionary<int, Matrix>();
            int accum = 0;
            foreach (RoomData roomdat in level.rooms)
            {
                DungeonRoom data = GetRoomDataFromFile(roomdat.RoomDataPath);
                Rotations.Add(accum, data.Base_Rotation + roomdat.Rotation);
                Matrix transforms = Matrix.CreateScale(data.Scale) * Matrix.CreateRotationY(MathHelper.ToRadians(data.Base_Rotation + roomdat.Rotation)) * Matrix.CreateTranslation(roomdat.Position);
                dict.Add(accum++, transforms);
            }
            level.SetTransforms(dict);
            Dictionary<int, Matrix> dads = new Dictionary<int, Matrix>();
            accum = 0;
            foreach (Doodad dad in level.doodads)
            {
                dads.Add(accum++, Matrix.CreateScale(dad.Scale) * Matrix.CreateRotationY(MathHelper.ToRadians(dad.Rotation)) * Matrix.CreateTranslation(dad.Position));
            }
            level.SetDoodadTransforms(dads);
        }

        public List<StaticMesh> GetLevelPhysicsMeshes(Level level)
        {
            List<StaticMesh> meshes = new List<StaticMesh>();
            foreach (int index in level.GetModels().Keys)
            {
                Vector3[] vertices;
                int[] indices;
                TriangleMesh.GetVerticesAndIndicesFromModel(level.GetModels()[index], out vertices, out indices);
                var mesh = new StaticMesh(vertices, indices, new AffineTransform(new Vector3(0.03f), Quaternion.CreateFromRotationMatrix(Matrix.CreateRotationY(MathHelper.ToRadians(this.Rotations[index]))), level.rooms[index].Position + new Vector3(0, 1, 0)));
                meshes.Add(mesh);
            }
            return meshes;
        }

        public Level Generate()
        {
            List<string> rooms = new List<string>();
            //rooms.Add("1open");
            //rooms.Add("2open");
            rooms.Add("3open");
            rooms.Add("4open");
            //rooms.Add("hall");
            //rooms.Add("basicroom");

            Random rand = new Random();

            Level lev = new Level();

            for (int j = 0; j < 3; ++j)
            {
                for (int i = 0; i < 3; ++i)
                {
                    RoomData levelRoom = new RoomData();
                    levelRoom.RoomDataPath = rooms[rand.Next(0, rooms.Count)];
                    levelRoom.Rotation = 0.0f;
                    levelRoom.Position = new Vector3(0 + 7.6f * j, 0, 0 + 7.6f * i);
                    lev.rooms.Add(levelRoom);
                }
            }

            lev.doodads.Add(new Doodad("table_mortis-tenon_old-dirty", "table_mt_old-dirty_dif", new Vector3(0, 0, 2), 0.0f, 1.0f));
            lev.events.Add(new EventNode(new Vector3(0, 0, 2), 1.0f, true, false, "return true;", ""));

            GetLevelModels(lev);
            GetModelTextures(lev);
            GetLevelRoomTransforms(lev);

            SerializeLevel(lev, "Test");

            return lev;
        }


        internal Level LoadLevel(string levelName)
        {
            Level lev = DeserializeLevel(levelName);
            GetLevelModels(lev);
            GetModelTextures(lev);
            GetLevelRoomTransforms(lev);
            return lev;
        }
    }
}
