using System.Collections.Generic;
using Jint;
using Microsoft.Xna.Framework;

namespace RogueLike.Components
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class ScriptEngine : Microsoft.Xna.Framework.GameComponent
    {
        static JintEngine statEngine;
        JintEngine engine;
        public List<string> globalScripts;

        public ScriptEngine(Game game)
            : base(game)
        {
            if (statEngine == null)
                statEngine = new JintEngine();
            globalScripts = new List<string>();
            engine = new JintEngine();
        }

        public void SetParameter(string param, object value)
        {
            engine.SetParameter(param, value);
        }

        public void Add(string script)
        {
            globalScripts.Add(script);
        }

        public void Reset()
        {
            engine = new JintEngine();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        public object Run(string script)
        {
            return engine.Run(@script);
        }

        public static object RunStatic(string script)
        {
            return statEngine.Run(@script);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            foreach (string script in globalScripts)
            {
                engine.Run(script);
            }
            base.Update(gameTime);
        }
    }
}
