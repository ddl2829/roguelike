using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

//Implementation based on techniques described in the tutorials by Jamie McMahon on http://www.XnaGpa.net

namespace GameLibrary
{
    //The inputhandler contains static methods to check keyboard, gamepad, and mouse input
    public class InputHandler : Microsoft.Xna.Framework.GameComponent
    {
        static KeyboardState keyboardState;
        static KeyboardState lastKeyboardState;

        static MouseState mouseState;
        static MouseState lastMouseState;

        static GamePadState gamePadState;
        static GamePadState lastGamePadState;

        public static KeyboardState KeyboardState
        {
            get { return keyboardState; }
        }

        public static KeyboardState LastKeyboardState
        {
            get { return lastKeyboardState; }
        }

        public InputHandler(Game game)
            : base(game)
        {
            mouseState = Mouse.GetState();
            keyboardState = Keyboard.GetState();
        }

        public override void Initialize()
        {
            base.Initialize();
        }


        //Updates the states of the various game input devices
        public override void Update(GameTime gameTime)
        {
            lastKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            lastMouseState = mouseState;
            mouseState = Mouse.GetState();

            lastGamePadState = gamePadState;
            gamePadState = GamePad.GetState(PlayerIndex.One);
            
            base.Update(gameTime);
        }

        public static void Flush()
        {
            lastKeyboardState = keyboardState;
            lastMouseState = mouseState;
        }

        //Returns true if a key was just released
        public static bool KeyReleased(Keys key)
        {
            return keyboardState.IsKeyUp(key) && lastKeyboardState.IsKeyDown(key);
        }

        //Returns true if a key was just pressed
        public static bool KeyPressed(Keys key)
        {
            return keyboardState.IsKeyDown(key) && lastKeyboardState.IsKeyUp(key);
        }

        //Returns true if a key is being held down
        public static bool KeyDown(Keys key)
        {
            return keyboardState.IsKeyDown(key);
        }

        //Returns true if the left mouse button was clicked
        public static bool MouseLeftClicked()
        {
            return (mouseState.LeftButton == ButtonState.Pressed) && (lastMouseState.LeftButton == ButtonState.Released);
        }

        public static bool MouseLeftButtonDown()
        {
            return mouseState.LeftButton == ButtonState.Pressed;
        }

        public static bool MouseRightClicked()
        {
            return (mouseState.RightButton == ButtonState.Pressed) && (lastMouseState.RightButton == ButtonState.Released);
        }

        //Returns a vector2 containing the mouse position
        public static Vector2 MousePosition()
        {
            return new Vector2(mouseState.X, mouseState.Y);
        }

        //Returns true if the gamepad is connected
        public static bool GamePadConnected()
        {
            return gamePadState.IsConnected;
        }

        //Returns true if the button is down
        public static bool ButtonDown(Buttons button)
        {
            return gamePadState.IsButtonDown(button);
        }

        //Returns true if the button is up
        public static bool ButtonUp(Buttons button)
        {
            return gamePadState.IsButtonUp(button);
        }

        //Return true if a button was pressed between these states
        public static bool ButtonPressed(Buttons button)
        {
            return lastGamePadState.IsButtonUp(button) && gamePadState.IsButtonDown(button);
        }

        //Returns true if a button was released between states
        public static bool ButtonReleased(Buttons button)
        {
            return lastGamePadState.IsButtonDown(button) && gamePadState.IsButtonUp(button);
        }

        //Vibrates the game pad a specified amount
        public static bool VibrateGamePad(float amountLeft, float amountRight)
        {
            return GamePad.SetVibration(PlayerIndex.One, MathHelper.Clamp(amountLeft, 0.0f, 1.0f), MathHelper.Clamp(amountRight, 0.0f, 1.0f));
        }

        public static GamePadTriggers TriggerState()
        {
            return gamePadState.Triggers;
        }

        public static GamePadThumbSticks ThumbSticksState()
        {
            return gamePadState.ThumbSticks;
        }
    }
}
