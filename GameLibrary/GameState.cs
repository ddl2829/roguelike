using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
//Implementation based on techniques described in the tutorials by Jamie McMahon on http://www.XnaGpa.net
namespace GameLibrary
{
    public abstract class GameState : Microsoft.Xna.Framework.DrawableGameComponent
    {
        List<GameComponent> childComponents;

        bool accept_input = true;
        public bool AcceptInput
        {
            get { return accept_input; }
            set { accept_input = value; }
        }

        public List<GameComponent> Components
        {
            get { return childComponents; }
        }

        GameState tag;
        public GameState Tag
        {
            get { return tag; }
        }

        protected GameStateManager StateManager;

        public GameState(Game game, GameStateManager manager)
            : base(game)
        {
            StateManager = manager;
            childComponents = new List<GameComponent>();
            tag = this;
        }

        public override void Initialize()
        {
            base.Initialize();
            //Manually initialize child components- drawable components will have their loadcontent method called automatically
            foreach (GameComponent component in Components)
            {
                component.Initialize();
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameComponent component in childComponents)
            {
                if (component.Enabled)
                    component.Update(gameTime);
            }
            base.Update(gameTime);
        }

        internal protected virtual void StateChange(object sender, EventArgs e)
        {
            if (StateManager.CurrentState == Tag)
                Show();
            else
                Hide();
        }

        public override void Draw(GameTime gameTime)
        {
            DrawableGameComponent drawComponent;
            foreach (GameComponent component in childComponents)
            {
                if (component is DrawableGameComponent)
                {
                    drawComponent = component as DrawableGameComponent;
                    if (drawComponent.Visible)
                        drawComponent.Draw(gameTime);
                }
            }
            base.Draw(gameTime);
        }

        protected virtual void Show()
        {
            Visible = true;
            Enabled = true;
            foreach (GameComponent component in childComponents)
            {
                component.Enabled = true;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = true;
            }
        }

        protected virtual void Hide()
        {
            Visible = false;
            Enabled = false;
            foreach (GameComponent component in childComponents)
            {
                component.Enabled = false;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = false;
            }
        }
    }
}
