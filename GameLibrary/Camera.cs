﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

//Implementation not used in game but derived from Paul's camera class

namespace GameLibrary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Camera : Microsoft.Xna.Framework.GameComponent
    {
        // view matrix
        public Matrix view;

        // camera position 
        private Vector3 position = new Vector3(0, -2, 2.0f);

        // Vector3 used to store yaw, pitch and roll (roll is not used)
        private Vector3 angle = new Vector3();

        // scale factor for movement speed
        private float speed = 0.05f;
        // scale factor for turning speed
        private float turnSpeed = 90f;

        int centerX = 0, centerY = 0;

        // store previous mouse state
        MouseState prevMouseState;


        public Camera(Game game)
            : base(game)
        {
        }


        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            // Set mouse position and do initial get state

            centerX = Game.Window.ClientBounds.Width / 2;
            centerY = Game.Window.ClientBounds.Height / 2;

            prevMouseState = Mouse.GetState();

            Mouse.SetPosition(Game.Window.ClientBounds.Width / 2,
                Game.Window.ClientBounds.Height / 2);

            base.Initialize();
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            centerX = Game.Window.ClientBounds.Width / 2;
            centerY = Game.Window.ClientBounds.Height / 2;

            MouseState mouse = Mouse.GetState();

                if (mouse.MiddleButton == ButtonState.Pressed) { base.Update(gameTime); return; }

                // update yaw and pitch angles
                angle.X += MathHelper.ToRadians((mouse.Y - centerY) * turnSpeed * 0.001f); // pitch
                angle.Y += MathHelper.ToRadians((mouse.X - centerX) * turnSpeed * 0.001f); // yaw

                // compute forward and side vectors (they are orthogonal to each other)
                Vector3 forward = Vector3.Transform(new Vector3(0, 0, 1.0f), Matrix.CreateRotationX(-angle.X) * Matrix.CreateRotationY(-angle.Y));
                Vector3 left = Vector3.Transform(new Vector3(1.0f, 0, 0f), Matrix.CreateRotationX(-angle.X) * Matrix.CreateRotationY(-angle.Y));

                // handle keyboard input

                if (InputHandler.KeyDown(Keys.W))
                    position += forward * speed;
                if (InputHandler.KeyDown(Keys.S))
                    position -= forward * speed;

                if (InputHandler.KeyDown(Keys.A))
                    position += left * speed;
                if (InputHandler.KeyDown(Keys.D))
                    position -= left * speed;


                // compute view matrix
                view = Matrix.Identity;
                view *= Matrix.CreateTranslation(position);
                view *= Matrix.CreateRotationZ(angle.Z);
                view *= Matrix.CreateRotationY(angle.Y);
                view *= Matrix.CreateRotationX(angle.X);


            // store old mouse position
            prevMouseState = mouse;
            // reset position of mouse to center of window
            Mouse.SetPosition(centerX, centerY);

            base.Update(gameTime);
        }
    }
}
