﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//Implementation based on techniques described in the tutorials by Jamie McMahon on http://www.XnaGpa.net
namespace GameLibrary.Controls
{
    public class LinkLabel : Control
    {
        public Color SelectedColor = Color.Red;

        public LinkLabel()
        {
            TabStop = true;
            HasFocus = false;
            Position = Vector2.Zero;
        }

        public override void Update(GameTime gameTime)
        {
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (HasFocus)
                spriteBatch.DrawString(SpriteFont, Text, Position, SelectedColor);
            else
                spriteBatch.DrawString(SpriteFont, Text, Position, Color);
        }

        public override void HandleInput()
        {
            if(!HasFocus)
                return;
            if (InputHandler.KeyReleased(Keys.Enter))
                base.OnSelected(null);                
        }
    }
}
