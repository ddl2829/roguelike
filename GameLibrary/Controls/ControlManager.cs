﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//Implementation based on techniques described in the tutorials by Jamie McMahon on http://www.XnaGpa.net
namespace GameLibrary.Controls
{
    public class ControlManager : List<Control>
    {
        public int SelectedControl = 0;
        public static SpriteFont SpriteFont;
        public bool AcceptInput = true;

        public event EventHandler FocusChanged;

        public ControlManager(SpriteFont spriteFont)
            : base()
        {
            ControlManager.SpriteFont = spriteFont;
        }

        public ControlManager(SpriteFont spriteFont, int capacity)
            : base(capacity)
        {
            ControlManager.SpriteFont = spriteFont;
        }

        public ControlManager(SpriteFont spriteFont, IEnumerable<Control> collection)
            : base(collection)
        {
            ControlManager.SpriteFont = spriteFont;
        }

        public void Update(GameTime gameTime)
        {
            if (Count == 0)
                return;
            foreach (Control c in this)
            {
                if (c.Enabled)
                    c.Update(gameTime);
                if (c.HasFocus)
                    c.HandleInput();
            }

            if (!AcceptInput)
                return;

            if (InputHandler.KeyPressed(Keys.Up))
                PreviousControl();
            if (InputHandler.KeyPressed(Keys.Down))
                NextControl();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Control c in this)
            {
                if (c.Visible)
                    c.Draw(spriteBatch);
            }
        }

        public void NextControl()
        {
            if (Count == 0)
                return;

            int currentControl = SelectedControl;

            this[SelectedControl].HasFocus = false;
            do
            {
                ++SelectedControl;
                if (SelectedControl == Count)
                    SelectedControl = 0;
                if (this[SelectedControl].TabStop && this[SelectedControl].Enabled)
                {
                    if (FocusChanged != null)
                        FocusChanged(this[SelectedControl], null);
                    break;
                }
            } while (currentControl != SelectedControl);
            this[SelectedControl].HasFocus = true;
        }

        public void PreviousControl()
        {
            if (Count == 0)
                return;
            int currentControl = SelectedControl;
            this[SelectedControl].HasFocus = false;
            do
            {
                --SelectedControl;
                if (SelectedControl < 0)
                    SelectedControl = Count - 1;
                if (this[SelectedControl].TabStop && this[SelectedControl].Enabled)
                {
                    if (FocusChanged != null)
                        FocusChanged(this[SelectedControl], null);
                    break;
                }
            } while (currentControl != SelectedControl);
            this[SelectedControl].HasFocus = true;
        }
    }
}
