﻿using Microsoft.Xna.Framework;

namespace XRogueLibrary
{
    public class EventNode
    {
        //Node Types:
        //  0: onEnter
        //  1: onExit
        //  2: Action
        public int Type;
        public Vector3 Position;
        public float Radius;

        public bool runOnce;
        public bool ran;
        public bool near;

        public string onEnter;
        public string onExit;
        public string onAction;

        public BoundingSphere BoundingSphere
        {
            get { return new BoundingSphere(Position, Radius); }
        }

        public EventNode()
        {
            
        }

        public EventNode(Vector3 pos, float rad, bool runonce, bool ran, string onenter, string onexit)
        {
            Position = pos;
            Radius = rad;
            runOnce = runonce;
            this.ran = ran;
            onEnter = onenter;
            onExit = onexit;
            near = false;
        }
    }
}
