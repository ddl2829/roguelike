﻿using Microsoft.Xna.Framework;

namespace RogueLike.Components.NpcClasses
{
    public class NpcData
    {
        public string Data;
        public Vector3 Position;
    }
}
