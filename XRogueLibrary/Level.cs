﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueLike.Components.NpcClasses;

namespace XRogueLibrary
{
    public struct RoomData {
        public string RoomDataPath;
        public Vector3 Position;
        public float Rotation;
    };

    public class Level
    {
        public List<RoomData> rooms;
        public List<Doodad> doodads;
        public List<EventNode> events;
        public List<NpcData> npcs;

        /*Level Engine Dictionaries*/
        private Dictionary<int, Model> Models;
        private Dictionary<int, Matrix> Transforms;
        private Dictionary<int, Texture2D> Textures;
        private Dictionary<int, Model> Doodads;
        private Dictionary<int, Matrix> DoodadTransforms;
        private Dictionary<int, Texture2D> DoodadTextures;
        /*End Level Engine*/

        public Dictionary<int, Model> GetModels()
        {
            return Models;
        }

        public Dictionary<int, Model> GetDoodads()
        {
            return Doodads;
        }

        public void SetDoodadTextures(Dictionary<int, Texture2D> dic)
        {
            DoodadTextures = dic;
        }

        public void SetDoodadModels(Dictionary<int, Model> dic)
        {
            Doodads = dic;
        }

        public void SetDoodadTransforms(Dictionary<int, Matrix> dic)
        {
            DoodadTransforms = dic;
        }

        public void SetModels(Dictionary<int, Model> dic)
        {
            Models = dic;
        }

        public void SetTransforms(Dictionary<int, Matrix> dic)
        {
            Transforms = dic;
        }

        public void SetTextures(Dictionary<int, Texture2D> dic)
        {
            Textures = dic;
        }

        public Level()
        {
            rooms = new List<RoomData>();
            doodads = new List<Doodad>();
            events = new List<EventNode>();
        }


        public void Draw(Effect levelEffect)
        {
            int index = 0;
            while (index < rooms.Count)
            {
                Matrix[] bones = new Matrix[Models[index].Bones.Count];
                Models[index].CopyAbsoluteBoneTransformsTo(bones);
                levelEffect.Parameters["ModelTexture"].SetValue(Textures[index]);

                levelEffect.CurrentTechnique = levelEffect.Techniques["PointLight_World"];
                foreach (ModelMesh mesh in Models[index].Meshes)
                {
                    levelEffect.Parameters["World"].SetValue(bones[mesh.ParentBone.Index] * Transforms[index]);
                    foreach (ModelMeshPart part in mesh.MeshParts)
                    {
                        part.Effect = levelEffect;
                    }
                    foreach (Effect effect in mesh.Effects)
                    {
                        foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                        {
                            pass.Apply();
                        }
                    }
                    mesh.Draw();
                }
                ++index;
            }
            index = 0;
            while (index < doodads.Count)
            {
                if (Doodads[index] != null)
                {
                    Matrix[] bones = new Matrix[Doodads[index].Bones.Count];
                    Doodads[index].CopyAbsoluteBoneTransformsTo(bones);
                    levelEffect.Parameters["ModelTexture"].SetValue(DoodadTextures[index]);
                    levelEffect.CurrentTechnique = levelEffect.Techniques["PointLight_World"];
                    foreach (ModelMesh mesh in Doodads[index].Meshes)
                    {
                        levelEffect.Parameters["World"].SetValue(bones[mesh.ParentBone.Index] * DoodadTransforms[index]);
                        foreach (ModelMeshPart part in mesh.MeshParts)
                        {
                            part.Effect = levelEffect;
                        }
                        foreach (Effect effect in mesh.Effects)
                        {
                            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                            {
                                pass.Apply();
                            }
                        }
                        mesh.Draw();
                    }
                }
                ++index;
            }
        }
    }
}
