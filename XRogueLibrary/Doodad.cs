﻿using Microsoft.Xna.Framework;

namespace XRogueLibrary
{
    public class Doodad
    {
        public string Model;
        public string Texture;
        public Vector3 Position;
        public float Rotation;
        public float Scale;

        public Doodad()
        {
            Model = "";
            Position = Vector3.Zero;
            Rotation = 0.0f;
        }

        public Doodad(string Mesh, string Texture, Vector3 Position, float Rotation, float Scale)
        {
            Model = Mesh;
            this.Texture = Texture;
            this.Position = Position;
            this.Rotation = Rotation;
            this.Scale = Scale;
        }
    }
}
