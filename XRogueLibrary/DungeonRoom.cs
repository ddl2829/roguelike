﻿
namespace XRogueLibrary
{
    public class DungeonRoom
    {
        public string Mesh;
        public float Scale;
        public float Base_Rotation;

        public DungeonRoom(string mesh, float scale, float rotation)
        {
            Mesh = mesh;
            Scale = scale;
            Base_Rotation = rotation;
        }

        public DungeonRoom()
        {

        }
    }
}
